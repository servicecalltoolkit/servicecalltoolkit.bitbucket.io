package org.protneut.testclient;

import java.util.List;
import java.util.Map;

import org.sct.clientframework.Service;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.sct.clientframework.utils.ParameterBuilder;
import org.sct.clientframework.utils.ResponseMiner;
import org.springframework.web.client.RestClientException;

public class TestClient {

	
	public void get_return_String__String() throws  RestClientException, ClientFrameworkException, ServerException{
		String customerId = "ksdf87432hjdsf879";
		
		String buildingName = Service.createInstance("service_consumer", "service_consumer").get("postcode->buildingName", customerId);
		
		System.out.println("buildingName: "+buildingName);
	}
	
	
	public void get_return_Map__String() throws ClientFrameworkException, RestClientException, ServerException {
		String customerId = "2";
		
		Map<String, ?> customerAddress = Service.createInstance("service_consumer", "service_consumer").getStruct("postcode->fullAddress", customerId, Map.class);
		
		Object identifierType = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType", customerAddress);
		System.out.println(identifierType);
	}

	
	public void get_REST(int i) throws ClientFrameworkException, RestClientException, ServerException {
		Map<String, ?> customerAddress = Service.createInstance("service_consumer", "service_consumer").getStruct("REST->displayAddress", Map.class);
		
		Object identifierType = ResponseMiner.get("/GetAddressForPostCodeResponse/postalAddress/displayAddress", customerAddress);
		System.out.println(identifierType);
	}

	
	public void get_REST_only_list() throws ClientFrameworkException, RestClientException, ServerException {
		List<?> response = Service.createInstance("service_consumer", "service_consumer").getStruct("REST->list/value", List.class);
		
		Object values = ResponseMiner.get("/value", response);
		System.out.println(values);
	}
	

	public void get_return_String() throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");
		builder.add("country", "HU");
		builder.add("addressIdentification/identifierType", "nemtom");
		builder.add("addressIdentification/addressIdentifier", "eztse");
		
		String id = Service.createInstance("service_consumer", "service_consumer").get("responseHeader-id", builder.getParams());
		System.out.println(id);
	}
	
	
	public void get_return_401() throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");
		builder.add("country", "HU");
		
		String id = Service.createInstance("service_consumer", "service_consumer").get("REST->displayAddress_401", builder.getParams());
		System.out.println(id);
	}

	
	public void get_return_REST_simpleString() throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");
		builder.add("country", "HU");
		
		String sg = Service.createInstance("service_consumer", "service_consumer").get("REST->simpleString", builder.getParams());
		System.out.println(sg);
	} 
	
	
	public void get_return_REST_urlVariables(int i) throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("county", "Dorset");
		builder.add("town", "Bournemouth");
		
		Map<String,Object> identifierType = Service.createInstance("service_consumer", "service_consumer").getStruct("REST->url_variables->identifierType", builder.getParams(), Map.class);
		System.out.println(identifierType);
	} 
	
	
	public void get_return_ListOfMap__Map(int i) throws ClientFrameworkException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");
		builder.add("country", "HU");
		builder.add("addressIdentification/identifierType", "nemtom");
		builder.add("addressIdentification/addressIdentifier", "eztse");
		
		Service service = Service.createInstance("service_consumer", "service_consumer");
		service.setEncryptionKey("Bar12345Bar12345");
		service.setEncryptionNeeded(true);
		
		Map<String, ?> adresses = service.getStruct("postcode->some_strange_value", builder.getParams(),Map.class);
		Object addressIdentification = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification", adresses);
		System.out.println(addressIdentification);
		Object identifierType = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType", adresses);
		System.out.println(identifierType);
	}

	
	public void get_from_DB_by_serviceTye(int i) throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("successful", "1");
		
		Map<String,List<String>> services = Service.createInstance("service_consumer", "service_consumer").getStruct("DB-protneut->WS-service-data", builder.getParams(), Map.class);
		System.out.println(services.get("CNT").get(2));
	}
	
}
